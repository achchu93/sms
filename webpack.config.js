var webpack = require('webpack');
module.exports = {
    entry: __dirname + '/resources/assets/js/app.js',
    output: {
        path: __dirname + '/public/js',
        filename: 'app.bundle.js',
        publicPath: "/js/"
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"
            },

            {
                test:/\.vue$/, loader: "vue-loader"
            },
            {
                test: /\.css$/,
                loaders: ['style-loader?sourceMap', 'css-loader?sourceMap', 'resolve-url-loader']
            },
            {
                test: /\.woff($|\?)|\.jpg($|\?)|\.png($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loader: 'url-loader'
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader?sourceMap", "css-loader?sourceMap", 'resolve-url-loader', "sass-loader"]
            }
        ]
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),

        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            moment : "moment"
        }),

        new webpack.optimize.OccurrenceOrderPlugin(),
    ]
};