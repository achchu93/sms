<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$c = 'App\Http\Controllers\\';

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) use ($c) {
    $api->get('test', function () {
        dd("working");
    });
    $api->post('login', $c.'Auth\LoginController@login');
});

$api->version('v1', ['middleware' => 'jwt.auth'],function ($api) use ($c) {
    $api->get('courses', $c.'CourseController@getAllCourses');
});