<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = \App\Permission::create([
            'name'  => 'delete_topic',
            'label' =>  'user can delete topic'
        ]);
    }
}
