<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Role::create([
            'name'  =>  'super-admin',
            'label' =>  'The admin of system'
        ]);

        $permission = \App\Permission::whereName('edit_topic')->first();

        $role->assign($permission);
    }
}
