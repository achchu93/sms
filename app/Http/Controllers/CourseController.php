<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function getAllCourses()
    {
        $courses = Course::all();
        return $this->response->array($courses);
    }
}
