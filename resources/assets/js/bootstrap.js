
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';

var VueResource = require('vue-resource');

export const bootstrap = function (Vue) {
    Vue.use(VueResource);
    Vue.use(VeeValidate);
    require("../../assets/sass/app.scss");
    require("jquery");
    require("moment");
    require("../../../template/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js");
    require("../../../template/html/scripts/app.js");
};