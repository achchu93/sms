import * as types from '../types';
import * as actions from '../actions/course.actions';

const state = {
    courses : [],
};

const mutations = {
    [types.GET_COURSES] (state, courses){
        state.courses = courses;
    }
};

export default {
    state,
    mutations,
    actions
}