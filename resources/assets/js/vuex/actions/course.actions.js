import Vue from 'vue';
import * as types from '../types';

const resource = () => {
    var actions = {
    };
    return Vue.resource('courses{/id}', null, actions, {
        api_auth : true
    });
};

export const getAllCourses = function ({commit}, payload) {

    let success = payload.success;
    let error = payload.error;

    resource().get({}).then((response) => {
        commit(types.GET_COURSES, response.body.courses);
        if(success) success( response);
    }, (response) => {
        if(error) error( response);
    });

};