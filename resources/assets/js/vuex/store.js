import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import course from './modules/course';

Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        course
    },
    strict: debug
});
