import VueRouter from 'vue-router';
var VueAuth = require('@websanova/vue-auth');
import Welcome from './components/welcome.vue';
import Login from './components/auth/login.vue';
import Course from './components/course/courses.vue';

const routes = [
    {   path:'/welcome',
        component:Welcome,
        name:'Welcome'
    },
    {
        path:'/login',
        meta: {auth: false},
        component:Login,
        name:'Login'
    },
    {
        path:'/courses',
        meta: {auth: true},
        component:Course,
        name:'Courses'
    }
];


const initRoutes = function (Vue) {

    Vue.use(VueRouter);

    var router = new VueRouter({
        mode: 'history',
        routes: routes,
        linkActiveClass: 'active'
    });

    Vue.http.options.root = '/api';

    Vue.use(VueAuth, {
        authRedirect : {path: '/login'},
        registerData: {url: 'register', method: 'POST'},
        loginData: {url: 'login', method: 'POST', redirect: null},
        logoutData: { redirect: '/login',},
        fetchData: {url: 'user', method: 'GET'},
        router : router
    });

    // set auth token to auth needed routes. // //  // set it on the resource option parameter as {api_auth}
    Vue.http.interceptors.push((request, next) => {

        if(request.api_auth) {
            request.headers.set('Authorization', "Bearer " + router.app.$auth.token())
        }
        next();
    });

    return router;
};

export {initRoutes}
