import Vue from 'vue';
import {bootstrap} from './bootstrap';
import App from './components/app.vue';
import {initRoutes} from './routes';
import store from './vuex/store';

bootstrap(Vue);

const router = initRoutes(Vue);

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store
});
